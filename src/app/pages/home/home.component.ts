import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  //Image Icon

  public iconFb = '../../../assets/icons/facebook.png';

  public iconIG = '../../../assets/icons/twitter.png';

  public iconYT = '../../../assets/icons/youtube.png';

  constructor() { }

  ngOnInit(): void {
  }

}
